<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Regex as RegexValidator;

class UsersForm extends Form
{

    /**
     * Initialize the companies form
     */
    public function initialize($entity = null, $options = array())
    {

        if (!isset($options['edit'])) {
            $this->add(new Hidden("id"));
        } else {
            $this->add(new Hidden("id"));
        }

        // Name
        $name = new Text('name');
        $name->setLabel('Your Full Name');
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'Name is required'
            ))
        ));
        $this->add($name);

        // LastName
        $lastname = new Text('username');
        $lastname->setLabel('LastName');
        $lastname->addValidators(array(
            new PresenceOf(array(
                'message' => 'LastName is required'
            ))
        ));
        $this->add($lastname);

        // Email
        $email = new Text('email');
        $email->setLabel('E-Mail');
        $this->add($email);

        // Birth date
        $birthdate = new Date('birthdate');
        $birthdate->setLabel('Birth date');
        $this->add($birthdate);

        // Phone
        $phone = new Text("phone", array('placeholder' => '+7 (999) 999-99-99'));
        $phone->setLabel("Phone");
        $phone->addValidators(array(
            new RegexValidator(array(
                'pattern' => '/^(\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2})$/',
                'message' => 'Проверьте правильность заполнения телефонного номера : +7 (999) 999-99-99'
            ))
        ));
        $this->add($phone);

        $position = new Select('position_id', Position::find(), array(
            'using'      => array('id', 'title'),
            'useEmpty'   => true,
            'value' => !empty($entity->position_id) ? $entity->position_id : "",
            'emptyText'  => '...',
        ));
        $position->setLabel("Position");
        $this->add($position);

    }

}