<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class PositionController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for position
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Position", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $position = Position::find($parameters);
        if (count($position) == 0) {
            $this->flash->notice("The search did not find any position");

            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $position,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a position
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $position = Position::findFirstByid($id);
            if (!$position) {
                $this->flash->error("position was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "position",
                    "action" => "index"
                ));
            }

            $this->view->id = $position->id;

            $this->tag->setDefault("id", $position->id);
            $this->tag->setDefault("title", $position->title);
            
        }
    }

    /**
     * Creates a new position
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "index"
            ));
        }

        $position = new Position();

        $position->title = $this->request->getPost("title");
        

        if (!$position->save()) {
            foreach ($position->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "new"
            ));
        }

        $this->flash->success("position was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "position",
            "action" => "index"
        ));

    }

    /**
     * Saves a position edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $position = Position::findFirstByid($id);
        if (!$position) {
            $this->flash->error("position does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "index"
            ));
        }

        $position->title = $this->request->getPost("title");
        

        if (!$position->save()) {

            foreach ($position->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "edit",
                "params" => array($position->id)
            ));
        }

        $this->flash->success("position was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "position",
            "action" => "index"
        ));

    }

    /**
     * Deletes a position
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $position = Position::findFirstByid($id);
        if (!$position) {
            $this->flash->error("position was not found");

            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "index"
            ));
        }

        if (!$position->delete()) {

            foreach ($position->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "position",
                "action" => "search"
            ));
        }

        $this->flash->success("position was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "position",
            "action" => "index"
        ));
    }

}
