
{{ content() }}

<div align="right">
    {{ link_to("users/new", "Create users") }}
</div>

{{ form("users/search", "method":"post", "autocomplete" : "off") }}

<div align="center">
    <h1>Search users</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id">Id</label>
        </td>
        <td align="left">
            {{ text_field("id", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="username">Username</label>
        </td>
        <td align="left">
            {{ text_field("username", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="name">Name</label>
        </td>
        <td align="left">
            {{ text_field("name", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="email">Email</label>
        </td>
        <td align="left">
            {{ text_field("email", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="birthdate">Birthdate</label>
        </td>
        <td align="left">
                {{ text_field("birthdate", "type" : "date") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="phone">Phone</label>
        </td>
        <td align="left">
            {{ text_field("phone", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="position_id">Position</label>
        </td>
        <td align="left">
            {{ select('position_id', positions, 'using': ['id', 'title'], 'useEmpty': true, 'emptyText': 'Please, choose one...', 'emptyValue': '') }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Search") }}</td>
    </tr>
</table>

</form>
