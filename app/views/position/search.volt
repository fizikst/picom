
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("position/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("position/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for position in page.items %}
        <tr>
            <td>{{ position.id }}</td>
            <td>{{ position.title }}</td>
            <td>{{ link_to("position/edit/"~position.id, "Edit") }}</td>
            <td>{{ link_to("position/delete/"~position.id, "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("position/search", "First") }}</td>
                        <td>{{ link_to("position/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("position/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("position/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
