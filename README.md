# Picom

## Get Started

### Requirements

* PHP >= 5.4
* [Apache][2] Web Server with [mod_rewrite][3] enabled or [Nginx][4] Web Server
* Latest stable [Phalcon Framework release][5] extension enabled
* [MySQL][6] >= 5.1.5
* [phalcon-devtools][7]

### Installation

First you need to clone this repository:

```
$ git clone https://bitbucket.org/fizikst/picom
```

Run a Migration:

```sh
$ phalcon-devtools/phalcon migration run
```

Also you can override application config by creating `app/config/config.ini.dev` (already gitignored).


[1]: https://phalconphp.com/
[2]: http://httpd.apache.org/
[3]: http://httpd.apache.org/docs/current/mod/mod_rewrite.html
[4]: http://nginx.org/
[5]: https://github.com/phalcon/cphalcon/releases
[6]: https://www.mysql.com/
[7]: https://github.com/phalcon/phalcon-devtools 
